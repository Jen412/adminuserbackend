<?php

namespace App\Http\Controllers;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return User::all();
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        User::factory()->create([
            "email"=>$request->email,
            "apellidoP"=>$request->apellidoP,
            "apellidoM"=>$request->apellidoM,
            "telefono"=>$request->telefono,
            "tipoUsuario"=>$request->tipoUsuario
        ]);
        return response()->json([
            "mensaje"=>"Usuario Creado Correctamente",
            "email"=>$request->email,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return User::query()->find($user->id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $user->email= $request->email ?? $user->email;
        $user->password= $request->password ?? $user->password;
        $user->apellidoP= $request->apellidoP ?? $user->apellidoP;
        $user->apellidoM= $request->apellidoM ?? $user->apellidoM;
        $user->telefono= $request->telefono ?? $user->telefono;
        $user->updated_at = Carbon::now();
        $user->save();
        return response()->json([
            "mensaje"=>"Usuario Modificado correctamente",
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        User::destroy($user->id);
        return response()->json([
            "mensaje"=>"Usuario Eliminado correctamente"
        ]);
    }
}
