<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Direction;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Direction>
 */
class DirectionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'calle'=> fake()->streetName(),
            'colinia'=> fake()->sentence(),
            'numCasa'=> fake()->numberBetween(1,200),
            'codigoPostal'=> (string) (fake()->numberBetween(10000,50000)),
            'municipio'=>fake()->city(),
            'estado'=>fake()->sentence(),
            'user_id'=>function(){
                return User::query()->inRandomOrder()->first()->id;
            }
        ];
    }
}
